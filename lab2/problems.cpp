#include <iostream>

using namespace std;

bool Xi(int x, int y)
{
	return x % y == 0;
}

int g0(int x)
{
	int n = 2;
	while (n < x && !Xi(x,n)) n++;
	return n;
}

bool Xi1(int x)
{
	if (x == g0(x))
		return true;
	else
		return false;
}

int g1(int x, int y){
	if (x > y)
		return  x - y;
	return 0;
}

int g2(int x, int y)
{
	return x + y;
}

int g3(int x, int y){
	int n = 0;;
	while(n < g1(y,x) + 1 && !Xi1(g2(x,n))) n++;
	return n;
}

int g4(int x, int y)
{
	return g2(x, g3(x, y));
}

int Xi2(int x, int y){
	if (g4(x,y) == y + 1)
		return 0;
	return 1;
}

int sg(int x, int y)
{
	return Xi2(x,y)*g4(x,y);
}

int main(){
	int x , y;
	cin >> x >> y;
	cout << sg(x,y) << endl;
}
