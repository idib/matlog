package lab5;

public class Translate {
    boolean f;
    public int[] link;

    public Translate() {
        f = false;
    }

    public Translate(int[] link) {
        f = true;
        this.link = link.clone();
    }

    public boolean IsIncorrect() {
        if (f)
            return true;
        return false;
    }

    public boolean IsCorrect() {
        if (f)
            return true;
        return false;
    }

    @Override
    public String toString() {
        if (IsCorrect()) {
            String res = "Correct \n" +
                    "\nLinks =";
            for (int i = 0; i < link.length; i++) {
                res += " " + link[i];
            }
            return res;
        }
        return "Incorrect";
    }
}
