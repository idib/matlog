package lab5;

import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Text;

import java.util.ArrayList;

public class Vertex extends Group {
    static public double raduius = 15;
    public double mouseAnchorX;
    public double mouseAnchorY;
    public double initialTranslateX;
    public double initialTranslateY;
    private boolean subV = false;
    private Text Txt;
    private ArrayList<Edge> e;
    private Ellipse el;

    public Vertex() {
        el = new Ellipse(0, 0, raduius, raduius);
        Txt = new Text((subV ? -1 : 1) * raduius * 2, raduius, "");
        e = new ArrayList<>();
        getChildren().add(el);
        getChildren().add(Txt);
    }

    public Vertex(double centerX, double centerY, String n) {
        super();
        setTranslateX(centerX);
        setTranslateY(centerY);
    }

    public Point2D getCoord() {
        return new Point2D(getTranslateX(), getTranslateY());
    }

    public void refresh() {
        for (Edge edge : e) {
            edge.refresh();
        }
    }

    public void add(Edge r) {
        e.add(r);
    }

    public void setTranslateXY(double x, double y) {
        setTranslateX(x);
        setTranslateY(y);
        refresh();
    }

    public void setTranslateXY(Point2D p) {
        setTranslateXY(p.getX(), p.getY());
    }

    public void setFill(Paint value) {
        el.setFill(value);
    }

    public void setSubV(boolean fl) {
        subV = fl;
        Txt.setX(-1 * raduius * 2);
    }

    public void setName(String name) {
        Txt.setText(name);
    }

    @Override
    public String toString() {
        return Double.toString(getTranslateX()) + " " + Double.toString(getTranslateY());
    }
}
