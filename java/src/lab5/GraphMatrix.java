package lab5;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by idib on 07.12.16.
 */
public class GraphMatrix {
    boolean[][] g;
    Vertex[] Vert;
    ArrayList<Node> View;
    Color ColorVertex;
    int n;
    double sx = 15, sy = 15;

    public GraphMatrix(int n) {
        g = new boolean[n][n];
        for (int i = 0; i < g.length; i++) {
            Arrays.fill(g[i], false);
        }
        this.n = n;
        Vert = new Vertex[n];
        for (int i = 0; i < Vert.length; i++) {
            Vert[i] = new Vertex();
            setV(i, sx + i * Vertex.raduius * 3, sy);
        }
    }

    public void set(int i, int j, boolean v) {
        if (i < n && j < n) {
            g[i][j] = v;
        }
    }

    public boolean get(int i, int j) {
        if (i < n && j < n) {
            return g[i][j];
        }
        return false;
    }

    public void setV(int i, double x, double y) {
        if (i < n) {
            Vert[i].setTranslateXY(x, y);
            Vert[i].setName(Integer.toString(i));
            Vert[i].setFill(ColorVertex);
        }
    }

    public void setColor(Color c) {
        for (Vertex vertex : Vert) {
            vertex.setFill(c);
        }
    }

    public void setSubV(boolean fl) {
        for (Vertex v : Vert) {
            v.setSubV(fl);
        }
    }

    private Translate permute(int[] link, boolean[] set, int depth, GraphMatrix subG) {
        Translate res;
        if (depth == subG.n) {
            boolean find = true;
            for (int i = 0; find && i < subG.n; i++)
                for (int j = 0; find && j < subG.n; j++)
                    if (subG.g[i][j] && !g[link[i]][link[j]])
                        find = false;
            if (find)
                return new Translate(link);
        } else
            for (int i = 0; i < n; i++)
                if (!set[i]) {
                    link[depth] = i;
                    set[i] = true;
                    res = permute(link, set, depth + 1, subG);
                    if (res.IsCorrect())
                        return res;
                    set[i] = false;
                }
        return new Translate();
    }

    public Translate findSubGraph(GraphMatrix subG) {
        if (subG.n > 0 && subG.n <= n)
            return permute(new int[subG.n], new boolean[n], 0, subG);
        return new Translate();
    }

    public ArrayList<Node> getNodes() {
        if (View == null) {
            View = new ArrayList<>();
            for (Vertex v : Vert) {
                View.add(makeDraggable(v));
            }
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (g[i][j]) {
                        View.add(new Edge(Vert[i], Vert[j]));
                    }
                }
            }
        }
        return View;
    }

    public void transplant(Translate t, GraphMatrix map) {
        if (t.IsCorrect()) {
            for (int i = 0; i < map.n; i++) {
                map.Vert[i].setTranslateXY(Vert[t.link[i]].getCoord());
            }
        }
    }

    private Node makeDraggable(final Vertex node) {
        final Group wrapGroup = new Group(node);
        wrapGroup.addEventFilter(
                MouseEvent.ANY,
                mouseEvent -> mouseEvent.consume());
        wrapGroup.addEventFilter(
                MouseEvent.MOUSE_PRESSED,
                mouseEvent -> {
                    node.mouseAnchorX = mouseEvent.getX();
                    node.mouseAnchorY = mouseEvent.getY();
                    node.initialTranslateX =
                            node.getTranslateX();
                    node.initialTranslateY =
                            node.getTranslateY();
                });
        wrapGroup.addEventFilter(
                MouseEvent.MOUSE_DRAGGED,
                mouseEvent -> {
                    // shift node from its initial position by delta
                    // calculated from mouse cursor movement
                    node.setTranslateXY(
                            node.initialTranslateX
                                    + mouseEvent.getX()
                                    - node.mouseAnchorX,
                            node.initialTranslateY
                                    + mouseEvent.getY()
                                    - node.mouseAnchorY);
                });
        return wrapGroup;
    }

    @Override
    public String toString() {
        String out = n + "\n";
        for (Vertex v : Vert) {
            out += v.toString() + '\n';
        }
        for (boolean[] b : g) {
            for (boolean v : b) {
                out += (v ? '1' : '0') + " ";
            }
            out += '\n';
        }
        return out;
    }
}