package lab5;

import javafx.scene.Group;
import javafx.scene.shape.Line;

import java.util.ArrayList;

/**
 * Created by idib on 07.12.16.
 */
public class Edge extends Group {
    private Vertex v1,v2;

    public Edge(Vertex v1, Vertex v2) {
        this.v1 = v1;
        this.v2 = v2;
        v1.add(this);
        v2.add(this);
        refresh();
    }

    private void getLP() {
        double sX = v1.getTranslateX();
        double sY = v1.getTranslateY();
        double eX = v2.getTranslateX();
        double eY = v2.getTranslateY();
        getChildren().clear();
        ArrayList<Line> res = new ArrayList<>();
        double deg = Math.atan2(eY - sY, eX - sX);
        double neX = Vertex.raduius * Math.cos(deg);
        double neY = Vertex.raduius * Math.sin(deg);
        res.add(new Line(eX - neX, eY - neY, sX + neX, sY + neY));
        deg -= Math.PI / 6;
        double nX = Vertex.raduius * Math.cos(deg);
        double nY = Vertex.raduius * Math.sin(deg);
        res.add(new Line(eX - neX - nX, eY - neY - nY, eX - neX, eY - neY));
        deg += Math.PI / 3;
        nX = Vertex.raduius * Math.cos(deg);
        nY = Vertex.raduius * Math.sin(deg);
        res.add(new Line(eX - neX - nX, eY - neY - nY, eX - neX, eY - neY));
        getChildren().addAll(res);
    }

    public void refresh(){
        getLP();
    }
}
