package lab5;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

public class Main extends Application {
    private GraphMatrix G1, G2;
    private ExtensionFilter filterInFile = new ExtensionFilter("data in", "*.in");
    private ExtensionFilter filterGraphFile = new ExtensionFilter("Graph", "*.graph");
    private Pane panelsGraph;

    public static void main(String[] args) throws FileNotFoundException {
        launch(args);
    }

    private GraphMatrix Read(File f) throws FileNotFoundException {
        Scanner in = new Scanner(f);
        String name = f.getName();
        if (name.length() - name.lastIndexOf(".in") == 3) {
            int n = in.nextInt();
            int m = in.nextInt();
            GraphMatrix g = new GraphMatrix(n);
            int a, b;
            for (int i = 0; i < m; i++) {
                a = in.nextInt();
                b = in.nextInt();
                g.set(a, b, true);
            }
            return g;
        }
        if (name.length() - name.lastIndexOf(".graph") == 6) {
            int n = in.nextInt();
            GraphMatrix g = new GraphMatrix(n);
            int temp;
            double x, y;
            for (int i = 0; i < n; i++) {
                x = Double.parseDouble(in.next());
                y = Double.parseDouble(in.next());
                g.setV(i, x, y);
            }
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    temp = in.nextInt();
                    if (temp == 1)
                        g.set(i, j, true);
                }
            }
            return g;
        }
        return null;
    }

    private void Write(File f, GraphMatrix g) throws IOException {
        String name = f.getName();
        if (name.length() - name.lastIndexOf(".graph") != 6) {
            f = new File(f.getAbsolutePath() + ".graph");
        }
        f.createNewFile();
        PrintStream out = new PrintStream(f);
        out.print(g.toString());
    }

    private GraphMatrix OperFileGraph(Stage stage) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().add(filterGraphFile);
        fileChooser.getExtensionFilters().add(filterInFile);
        fileChooser.setInitialDirectory(new File("src/lab5/test"));

        File f = fileChooser.showOpenDialog(stage);
        try {
            return Read(f);
        } catch (FileNotFoundException e) {
            System.out.println("No load");
        }
        return null;
    }

    private void SaveFileGraph(Stage stage, GraphMatrix g) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().add(filterGraphFile);

        File f = fileChooser.showSaveDialog(stage);
        try {
            Write(f, g);
        } catch (IOException e) {
            System.out.println("No Save");
        }
    }

    @Override
    public void start(final Stage stage) {
        Button readG1 = new Button();
        readG1.setText("загрузить граф 1");
        readG1.setOnAction(event -> {
            if (G1 != null)
                panelsGraph.getChildren().removeAll(G1.getNodes());
            G1 = OperFileGraph(stage);
            G1.setColor(Color.BLUE);
            System.out.println("load G1");
            panelsGraph.getChildren().addAll(G1.getNodes());
        });

        Button readG2 = new Button();
        readG2.setText("загрузить граф 2");
        readG2.setOnAction(event -> {
            if (G2 !=null)
                panelsGraph.getChildren().removeAll(G2.getNodes());
            G2 = OperFileGraph(stage);
            G2.setColor(Color.RED);
            G2.setSubV(true);
            System.out.println("load G2");
            panelsGraph.getChildren().addAll(G2.getNodes());
        });

        Button writeG1 = new Button();
        writeG1.setText("сохранить граф 1");
        writeG1.setOnAction(event -> {
            SaveFileGraph(stage, G1);
        });

        Button writeG2 = new Button();
        writeG2.setText("сохранить граф 2");
        writeG2.setOnAction(event -> {
            SaveFileGraph(stage, G2);
        });


        Button Find = new Button();
        Find.setText("Поиск");
        Find.setOnAction(event -> {
            Translate t = G1.findSubGraph(G2);
            G1.transplant(t, G2);
            System.out.println(t);
        });

        HBox panelButtons = new HBox();
        panelButtons.getChildren().add(readG1);
        panelButtons.getChildren().add(readG2);
        panelButtons.getChildren().add(writeG1);
        panelButtons.getChildren().add(writeG2);
        panelButtons.getChildren().add(Find);

        panelsGraph = new Pane();
        BorderPane.setAlignment(panelsGraph, Pos.TOP_LEFT);

        final BorderPane sceneRoot = new BorderPane();
        sceneRoot.setTop(panelButtons);
        sceneRoot.setCenter(panelsGraph);

        final Scene scene = new Scene(sceneRoot, 500, 400);
        stage.setScene(scene);
        stage.setTitle("isomorphic");
        stage.show();
    }

}
