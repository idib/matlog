package RR1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by idib on 09.12.16.
 */
public class RR1 {
    private static long[] read(String nameFileIN) {
        try {
            return Arrays.stream(new Scanner(new File(nameFileIN)).nextLine().split(" ")).mapToLong(Long::parseLong).toArray();
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    private static boolean tryDiv(long[] in) {
        for (int i = 1; i < in.length; i++) {
            long cur = in[i] / in[i - 1];
            long residue = in[i] % in[i - 1];
            if (residue != 0)
                return false;
        }
        return true;
    }

    private static boolean treONES(long[] in) {
        boolean fl = true;
        for (int i = 0; i < in.length; i++)
            if (in[i] != 1)
                return false;
        return true;
    }

    private static boolean correct(long[] in) {
        if (in.length == 1 && in[0] == 1)
            return true;
        for (int i = 0; i < in.length; i++) {
            if (in[i] < 1)
                return false;
        }
        return true;
    }

    private static long[] Div(long[] in) {
        long[] res = new long[in.length - 1];
        for (int i = 0; i < res.length; i++) {
            res[i] = in[i + 1] / in[i];
        }
        return res;
    }

    private static long[] Minus(long[] in) {
        long[] res = new long[in.length - 1];
        for (int i = 0; i < res.length; i++) {
            res[i] = in[i + 1] - in[i];
        }
        return res;
    }

    private static String[] strDiv(String[] in) {
        String[] res = new String[in.length - 1];
        for (int i = 0; i < res.length; i++) {
            res[i] = "(" + in[i + 1] + ")/(" + in[i] + ")";
        }
        return res;
    }

    private static String[] strMinus(String[] in) {
        String[] res = new String[in.length - 1];
        for (int i = 0; i < res.length; i++) {
            res[i] = "(" + in[i + 1] + ")-(" + in[i] + ")";
        }
        return res;
    }

    private static String strAns(int depth, ArrayList<String[]> strs, ArrayList<Boolean> acts) {
        if (depth == strs.size() - 1)
            return strs.get(depth)[strs.get(depth).length - 1];
        if (acts.get(depth))
            return strs.get(depth)[strs.get(depth).length - 1] + "*(" + strAns(depth + 1, strs, acts) + ')';
        else
            return strs.get(depth)[strs.get(depth).length - 1] + "+(" + strAns(depth + 1, strs, acts) + ')';
    }

    private static long Ans(int depth, ArrayList<long[]> nums, ArrayList<Boolean> acts) {
        if (depth == nums.size() - 1)
            return 1;
        if (acts.get(depth))
            return nums.get(depth)[nums.get(depth).length - 1] * Ans(depth + 1, nums, acts);
        else
            return nums.get(depth)[nums.get(depth).length - 1] + Ans(depth + 1, nums, acts);
    }

    public static long sol(long[] inNum) {
        int startN = inNum.length + 1;
        ArrayList<Boolean> act = new ArrayList<>();
        ArrayList<long[]> nums = new ArrayList<>();
        ArrayList<String[]> strs = new ArrayList<>();
        String[] curstr = new String[inNum.length];
        for (int i = 1; i <= inNum.length; i++) {
            curstr[i - 1] = "a" + i;
        }
        System.out.println(Arrays.toString(inNum));
        nums.add(inNum);
        strs.add(curstr);
        while (!treONES(inNum) && correct(inNum)) {
            if (tryDiv(inNum)) {
                curstr = strDiv(curstr);
                inNum = Div(inNum);
                act.add(true);
            } else {
                curstr = strMinus(curstr);
                inNum = Minus(inNum);
                act.add(false);
            }
            System.out.println(Arrays.toString(inNum));
            strs.add(curstr);
            nums.add(inNum);
        }
        if (treONES(inNum)) {
            String strNext = "a" + startN + "=";
            System.out.println(strNext + strAns(0, strs, act));
            System.out.println(strNext + Ans(0, nums, act));
            return Ans(0, nums, act);
        }
        return -1;
    }

    public static void getNexts(long[] r, int n) {
        long[] temp;
        int t;
        for (int i = 0; i < n; i++) {
            temp = new long[r.length + 1];
            for (int j = 0; j < r.length; j++) {
                temp[j] = r[j];
            }
            temp[r.length] = sol(r);
            r = temp;
            System.out.println(Arrays.toString(temp));
        }
    }

    public static void main(String[] args) {
//        long[] r = read("src/RR1/test/1.in");
//        long[] r = read("src/RR1/test/2.in");
        long[] r = read("src/RR1/test/3.in");
//        getNexts(r,20);
        sol(r);
    }
}
