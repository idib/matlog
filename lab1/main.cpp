#include <iostream>

using namespace std;

int countAvtomat1(int sum){
    if (sum < 0)
        return 0;
    if(sum == 0)
        return 1;
    return countAvtomat1(sum - 10) + countAvtomat1(sum - 5) + countAvtomat1(sum - 2) + countAvtomat1(sum - 1);
}

int countAvtomat2(int sum){
    int res = 1;
    if (sum >= 10)
        res += countAvtomat2(sum % 10) * (sum / 10);
    if (sum >= 5)
        res += countAvtomat2(sum % 5) * (sum / 5);
    if (sum >= 2)
        res += countAvtomat2(sum % 2) * (sum / 2);
    return res;

}


int countAvtomat21(int sum, int count){
	if (sum < 0)		return 0;
    if (count == 1 || sum == 0) 
    	return 1;
    int res = 0;
    if (count >= 10)
	    res += countAvtomat21(sum - 10, 10);
	if (count >= 5)
    	res += countAvtomat21(sum - 5, 5);
    if (count >= 2)
		res += countAvtomat21(sum - 2, 2);
	if (count >= 1)
		res += countAvtomat21(sum - 1, 1);
	return res;
}

int countPyramid(int n){
    if (n <= 1)
        return 1;
    int res = 1;
    for (int i = 1; i < n; i++){
        res += countPyramid(n - i);
    }
    return res;
}

int main()
{
    for(int i = 1; i <= 17; i++)
        cout <<"test "<< i << ' ' << countAvtomat21(i,10) << endl;

    for(int i = 1; i <= 17; i++)
        cout <<"test "<< i << ' ' << countPyramid(i) << endl;
    return 0;
}
